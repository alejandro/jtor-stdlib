Gem::Specification.new do |s|
  s.platform     = 'java'
  s.name         = 'jtor-stdlib'
  s.version      = '0.1.5'
  s.date         = '2016-03-05'
  s.summary      = 'stdlib for JtoR'
  s.description  = 'This gem will be included on the Gemfile of your translated JtoR project'
  s.authors      = ['Alejandro Rodríguez']
  s.email        = 'alejandroluis24@gmail.com'
  s.homepage     = 'https://gitlab.com/eReGeBe/jtor-stdlib'
  s.files        = Dir['{lib}/**/*.rb']
  s.license      = 'MIT'
end
