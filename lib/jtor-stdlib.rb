warn 'Loading jtor-stdlib in a non-JRuby interpreter' unless defined? JRUBY_VERSION

require 'java'

require 'jtor-stdlib/superobject'
require 'jtor-stdlib/base'
require 'jtor-stdlib/jtor_import'
