module Jtor
  module StdLib
    module Base
      def self.included(base)
        base.extend(ClassMethods)
      end

      def initialize(*args)
        method, constructor_call = self.class.lookup(:initialize, *args)
        # I really tried to avoid using eval fellows, but JRuby is very fixed
        # on `super` being called here first than anything, and it wouldn't
        # accept a `proc` calling `initialize` on `sup`, so this will have
        # to do for now.
        if method
          eval(constructor_call)
          instance_exec(*args, &method)
        else
          super
        end
        self.class._initializers.each do |initializer|
          instance_exec(*args, &initializer)
        end
      end

      def sup
        @_sup ||= SuperObject.new(self)
      end

      module ClassMethods
        def _lookup_table
          @_lookup_table ||= {}
        end

        def _lookup_table_static
          @_lookup_table_static ||= {}
        end

        def _constructor_calls
          @_constructor_calls ||= {}
        end

        def _initializers
          @initializers ||= []
        end

        # For field initializers
        def add_java_initializer(&block)
          _initializers << block
        end

        # Method overloading is not a ruby thing, so we implement a pseudo-lookup
        # mechanism for method based on the type/count of their args
        def add_java_method(name, param_types, &block)
          previously_defined = add_method_to_lookup(_lookup_table, name,
              param_types, &block)
          return if previously_defined
          define_method(name) do |*args|
            instance_exec(*args, &self.class.lookup(name, *args))
          end
        end

        def add_static_java_method(name, param_types, &block)
          previously_defined = add_method_to_lookup(_lookup_table_static, name,
              param_types, &block)
          return if previously_defined
          define_singleton_method(name) do |*args|
            instance_exec(*args, &lookup(name, *args))
          end
        end

        def add_java_constructor(param_types, constructor_call = nil, &block)
          add_method_to_lookup(_lookup_table, :initialize, param_types, &block)
          _constructor_calls[param_types] = constructor_call
        end

        def lookup(name, *args)
          if _lookup_table[name]
            _lookup_table[name].each do |param_types, method|
              # NOTE: This is an oversimplification of the way Java determines
              # which method to invoke. I may work further into this later (see
              # http://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.12)
              next unless params_match(param_types, args)
              return (if (constructor_call = _constructor_calls[param_types])
                [method, constructor_call]
              else
                method
              end)
            end
          end
          method_missing(name, *args)
        end

        def params_match(types, values)
          # TODO: Handle var args (...)
          return false if types.size != values.size
          types.each_with_index do |type, index|
            value = values[index]
            return false unless value.is_a?(type) || value.to_java.is_a?(type)
          end
          true
        end

        def add_method_to_lookup(lookup_table, name, param_types, &block)
          previously_defined = lookup_table[name]
          lookup_table[name] ||= {}
          lookup_table[name][param_types] = block
          previously_defined
        end
      end
    end
  end
end
