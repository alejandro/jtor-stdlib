module Jtor
  module Imports
    def jtor_import(import_string)
      packages = import_string.split('.')
      path = File.join(packages[0..-2], "#{packages.last}.rb")
      paths = Dir[path]
      if paths.any?
        # Our loading model exposes ourselves to loading child classes when
        # loading their parents, so we rescue those errors and ignore them
        # (when the child gets loaded everything will be fine).
        paths.each { |p| require File.expand_path(p) rescue (NameError; nil) }
      else
        java_import import_string
      end
    end
  end
end

include Jtor::Imports
