module Jtor
  module StdLib
    class SuperObject
      def initialize(obj)
        @obj = obj
      end

      def method_missing(symbol, *args, &block)
        superclass = @obj.class.superclass
        if superclass < Base # If it's a Jtor class
          superclass.lookup(@obj, symbol, *args)
        else # All other classes (from http://stackoverflow.com/a/1251199)
          superclass.instance_method(symbol).bind(@obj).call(*args, &block)
        end
      end
    end
  end
end
